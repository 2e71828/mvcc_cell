`mvcc_cell` is a multiversion concurrency control-based
transactional-memory system for Rust.

Transactions are fully isolated and serialized with respect
to controlled values:
* Each transaction sees a fixed snapshot as of its start time
* Any concurrent commits to cells that a transaction has accessed will
  prevent that transaction from committing

## Example Usage
```
use mvcc_cell::{Mvcc,MvccCell};
let mvcc = Mvcc::new();

// Create a transactional slot
let slot = MvccCell::new(&mvcc, Box::new(0));

// Start concurrent transactions
let mut t1 = mvcc.begin();
let mut t2 = mvcc.begin();
let ro = mvcc.begin();

// Uncommitted values are not visible outside the transaction
t1[&slot] = 42;
assert_eq!(t2[&slot], 0);

// First committer wins, regardless of actual modification order
t2[&slot] = 7;
assert!(t2.try_commit().is_ok());

// Failed commits return the transaction, so you can still read
// the computed values
let t1_fail = t1.try_commit().unwrap_err();
assert_eq!(t1_fail[&slot], 42);

// Transactions always read the values that were current when
// begin() was called.
assert_eq!(ro[&slot], 0);
assert_eq!(mvcc.begin()[&slot], 7);
```
