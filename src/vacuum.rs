use crate::*;

/// A snapshot of active transactions, for history cleanup
pub struct Vacuum {
    mvcc_id: Id,
    latest: TxnId,
    active: std::collections::BTreeSet<TxnId>
}

impl Vacuum {
    /// Takes a snapshot of the active transactions, for cleaning up history
    pub fn new(mvcc: &Mvcc)->Vacuum {
        Vacuum {
            mvcc_id: mvcc.id(),
            latest: *mvcc.current.lock(),
            active: mvcc.txns.keys()
        }
    }

    /// Drops unreachable values from `cell`'s history
    pub fn clean<T:?Sized>(&self, cell:&MvccCell<T>) {
        assert_eq!(self.mvcc_id, cell.0.mvcc_id);
        cell.0.history.lock().vacuum(self.latest, &self.active);
    }
}
